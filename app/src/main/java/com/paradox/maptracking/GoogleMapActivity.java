package com.paradox.maptracking;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.paradox.maptracking.data.DatabaseContract;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class GoogleMapActivity extends AppCompatActivity {

    private MapView mMapView;
    private GoogleMap googleMap;

    private double first_lat;
    private double first_lon;

    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_map);

        setSupportActionBar((Toolbar) findViewById(R.id.tToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("MAPA");

        mMapView = (MapView) findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        googleMap = mMapView.getMap();

        if (getIntent().getStringExtra("track_id") != null) {
            Cursor cursor = getContentResolver().query(DatabaseContract.LocationTable.CONTENT_URI, null, DatabaseContract.LocationTable.TRACK_ID + "=?", new String[]{String.valueOf(getIntent().getStringExtra("track_id"))}, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    count++;

                    int id = cursor.getInt(cursor.getColumnIndex(DatabaseContract.LocationTable.ID));
                    double lat = cursor.getDouble(cursor.getColumnIndex(DatabaseContract.LocationTable.LAT));
                    double lon = cursor.getDouble(cursor.getColumnIndex(DatabaseContract.LocationTable.LON));
                    String name = cursor.getString(cursor.getColumnIndex(DatabaseContract.LocationTable.NAME));

                    if (count == 1) {
                        first_lat = lat;
                        first_lon = lon;
                    }

                    MarkerOptions marker = new MarkerOptions().position(new LatLng(lat, lon)).title(name);

                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_CYAN));



                    googleMap.addMarker(marker);
                }
                cursor.close();
            }
        }

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(first_lat, first_lon)).zoom(10).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }
}
