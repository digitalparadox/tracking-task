package com.paradox.maptracking;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.paradox.maptracking.R;
import com.paradox.maptracking.data.DatabaseContract;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class RouteActivity extends AppCompatActivity implements LocationListener, View.OnClickListener {

    private String TAG = getClass().getName();

    private LocationManager locationManager;
    private Geocoder geocoder;

    private EditText etName;
    private ListView lvLocation;
    private FloatingActionButton fabRecord;

    private long track_id;
    private long location_id;
    private double last_lat;
    private double last_lon;
    private int count = 0;

    private boolean record_track = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        setSupportActionBar((Toolbar) findViewById(R.id.tToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        etName = (EditText) findViewById(R.id.etName);

        lvLocation = (ListView) findViewById(R.id.lvLocation);

        fabRecord = (FloatingActionButton) findViewById(R.id.fabRecord);
        fabRecord.setOnClickListener(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        geocoder = new Geocoder(this);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAccuracy(Criteria.POWER_MEDIUM);

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabRecord:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etName.getWindowToken(), 0);

                if (!etName.getText().toString().isEmpty()) {
                    if (!record_track) {
                        fabRecord.setImageResource(R.drawable.pause);
                        etName.setCursorVisible(false);

                        record_track = true;
                        saveTrackName();
                        updateList();
                    } else {
                        fabRecord.setImageResource(R.drawable.play_arrow);
                        record_track = false;
                    }
                } else {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(RouteActivity.this);
                    dialog.setMessage("Campo requerido.");
                    dialog.setCancelable(false);
                    dialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
                break;
        }
    }

    private void saveTrackName() {
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.RouteTable.NAME, etName.getText().toString());
        values.put(DatabaseContract.RouteTable.COUNT, 0);
        values.put(DatabaseContract.RouteTable.CREATED_ON, new Date().getTime());
        track_id = ContentUris.parseId(getContentResolver().insert(DatabaseContract.RouteTable.CONTENT_URI, values));
    }

    private void updateTrackCount() {
        count++;

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.RouteTable.COUNT, count);
        getContentResolver().update(DatabaseContract.RouteTable.CONTENT_URI, values, DatabaseContract.RouteTable.ID + "=?", new String[]{String.valueOf(track_id)});
    }

    private void saveLocation(String name, double lat, double lon) {
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.LocationTable.TRACK_ID, track_id);
        values.put(DatabaseContract.LocationTable.NAME, name);
        values.put(DatabaseContract.LocationTable.LAT, lat);
        values.put(DatabaseContract.LocationTable.LON, lon);
        values.put(DatabaseContract.LocationTable.CREATED_ON, new Date().getTime());
        location_id = ContentUris.parseId(getContentResolver().insert(DatabaseContract.LocationTable.CONTENT_URI, values));

        updateTrackCount();
    }

    private void updateList() {
        CursorAdapter adapter = new CursorAdapter(this, getContentResolver().query(DatabaseContract.LocationTable.CONTENT_URI, null, DatabaseContract.LocationTable.TRACK_ID + "=?", new String[]{String.valueOf(track_id)}, null)) {
            @Override
            public View newView(Context context, Cursor cursor, ViewGroup parent) {
                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                return layoutInflater.inflate(R.layout.listview_item_route, parent, false);
            }

            @Override
            public void bindView(View view, Context context, Cursor cursor) {
                TextView tvName = (TextView) view.findViewById(R.id.tvLocationName);
                TextView tvLat = (TextView) view.findViewById(R.id.tvLocationLat);
                TextView tvLon = (TextView) view.findViewById(R.id.tvLocationLon);
                TextView tvTime = (TextView) view.findViewById(R.id.tvLocationTime);

                tvName.setText(cursor.getString(cursor.getColumnIndex(DatabaseContract.LocationTable.NAME)));
                tvLat.setText(cursor.getString(cursor.getColumnIndex(DatabaseContract.LocationTable.LAT)));
                tvLon.setText(cursor.getString(cursor.getColumnIndex(DatabaseContract.LocationTable.LON)));
                Date time = new Date(cursor.getLong(cursor.getColumnIndex(DatabaseContract.LocationTable.CREATED_ON)));
                tvTime.setText(String.valueOf(time));
            }
        };

        lvLocation.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {

        Location last_loc = new Location("");
        last_loc.setLatitude(last_lat);
        last_loc.setLongitude(last_lon);

        Location loc = new Location("");
        loc.setLatitude(location.getLatitude());
        loc.setLongitude(location.getLongitude());

        if (last_loc.distanceTo(loc) > 5f+-2f) {

            if (record_track) {
                try {
                    List<Address> address = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    saveLocation(address.get(0).getFeatureName(), location.getLatitude(), location.getLongitude());
                    last_lat = location.getLatitude();
                    last_lon = location.getLongitude();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
