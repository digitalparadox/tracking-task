/*
 * Copyright 2015 Tinbytes Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.paradox.maptracking.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;

public class SimpleContentProvider extends ContentProvider {
    // Provide a mechanism to identify all the incoming uri patterns.
    private static final int ROUTE = 1;
    private static final int LOCATION = 2;
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        // /route
        uriMatcher.addURI(DatabaseContract.AUTHORITY, DatabaseContract.RouteTable.URI_PATH, ROUTE);
        // /location
        uriMatcher.addURI(DatabaseContract.AUTHORITY, DatabaseContract.LocationTable.URI_PATH, LOCATION);
    }

    private DatabaseHelper dh;

    public boolean onCreate() {
        dh = new DatabaseHelper(getContext());
        return true;
    }

    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor c;
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setDistinct(true);
        switch (uriMatcher.match(uri)) {
            case ROUTE:
                qb.setTables(DatabaseContract.RouteTable.TABLE_NAME);
                if (sortOrder == null)
                    sortOrder = DatabaseContract.RouteTable.CREATED_ON + " ASC";
                c = qb.query(dh.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case LOCATION:
                qb.setTables(DatabaseContract.LocationTable.TABLE_NAME);
                if (sortOrder == null)
                    sortOrder = DatabaseContract.LocationTable.CREATED_ON + " DESC";
                c = qb.query(dh.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (getContext() != null) {
            c.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return c;
    }

    private Uri doInsert(Uri uri, String tableName, Uri contentUri, ContentValues contentValues) {
        SQLiteDatabase db = dh.getWritableDatabase();
        long rowId = db.insert(tableName, null, contentValues);
        if (rowId > 0) {
            Uri insertedUri = ContentUris.withAppendedId(contentUri, rowId);
            if (getContext() != null) {
                getContext().getContentResolver().notifyChange(insertedUri, null);
            }
            return insertedUri;
        }
        throw new SQLException("Failed to insert row - " + uri);
    }

    public Uri insert(@NonNull Uri uri, ContentValues values) {
        if (values != null) {
            switch (uriMatcher.match(uri)) {
                case ROUTE:
                    return doInsert(uri, DatabaseContract.RouteTable.TABLE_NAME, DatabaseContract.RouteTable.CONTENT_URI, values);
                case LOCATION:
                    return doInsert(uri, DatabaseContract.LocationTable.TABLE_NAME, DatabaseContract.LocationTable.CONTENT_URI, values);
                default:
                    throw new IllegalArgumentException("Unknown URI " + uri);
            }
        }
        return null;
    }

    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dh.getWritableDatabase();
        int count;
        switch (uriMatcher.match(uri)) {
            case ROUTE:
                count = db.update(DatabaseContract.RouteTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            case LOCATION:
                count = db.update(DatabaseContract.LocationTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dh.getWritableDatabase();
        int count;
        switch (uriMatcher.match(uri)) {
            case ROUTE:
                StringBuilder in = new StringBuilder("(");
                Cursor c = db.query(DatabaseContract.RouteTable.TABLE_NAME, null, selection, selectionArgs, null, null, null);
                boolean moveToNext = c.moveToNext();
                while (moveToNext) {
                    in.append(c.getInt(c.getColumnIndex(DatabaseContract.RouteTable.ID)));
                    moveToNext = c.moveToNext();
                    if (moveToNext) {
                        in.append(",");
                    }
                }
                in.append(")");
                c.close();
                count = db.delete(DatabaseContract.RouteTable.TABLE_NAME, selection, selectionArgs);
                break;
            case LOCATION:
                in = new StringBuilder("(");
                c = db.query(DatabaseContract.LocationTable.TABLE_NAME, null, selection, selectionArgs, null, null, null);
                moveToNext = c.moveToNext();
                while (moveToNext) {
                    in.append(c.getInt(c.getColumnIndex(DatabaseContract.LocationTable.ID)));
                    moveToNext = c.moveToNext();
                    if (moveToNext) {
                        in.append(",");
                    }
                }
                in.append(")");
                c.close();
                count = db.delete(DatabaseContract.LocationTable.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case ROUTE:
                return DatabaseContract.RouteTable.CONTENT_TYPE;
            case LOCATION:
                return DatabaseContract.LocationTable.CONTENT_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }
}
