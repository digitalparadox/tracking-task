/*
 * Copyright 2015 Tinbytes Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.paradox.maptracking.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();

    public DatabaseHelper(Context context) {
        super(context, DatabaseContract.DB_NAME, null, DatabaseContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Route
        db.execSQL("CREATE TABLE " + DatabaseContract.RouteTable.TABLE_NAME + " (" +
                DatabaseContract.RouteTable.ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                DatabaseContract.RouteTable.NAME + " TEXT NOT NULL," +
                DatabaseContract.RouteTable.COUNT + " INTEGER NOT NULL," +
                DatabaseContract.RouteTable.CREATED_ON + " INTEGER NOT NULL)");
        // Location
        db.execSQL("CREATE TABLE " + DatabaseContract.LocationTable.TABLE_NAME + " (" +
                DatabaseContract.LocationTable.ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                DatabaseContract.LocationTable.TRACK_ID + " INTEGER NOT NULL," +
                DatabaseContract.LocationTable.NAME + " TEXT NOT NULL," +
                DatabaseContract.LocationTable.LAT + " TEXT NOT NULL," +
                DatabaseContract.LocationTable.LON + " TEXT NOT NULL," +
                DatabaseContract.LocationTable.CREATED_ON + " INTEGER NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.RouteTable.TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.LocationTable.TABLE_NAME);
        onCreate(db);
    }
}
