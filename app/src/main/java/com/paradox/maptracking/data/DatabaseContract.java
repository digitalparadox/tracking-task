/*
 * Copyright 2015 Tinbytes Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.paradox.maptracking.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {
    public static final String AUTHORITY = "com.paradox.maptracking.data.SimpleContentProvider";
    public static final String DB_NAME = "maptracking.db";
    public static final int DB_VERSION = 1;

    public interface RouteColumns {
        String ID = BaseColumns._ID;
        String NAME = "name";
        String COUNT = "count";
        String CREATED_ON = "created_on";
    }

    public static final class RouteTable implements RouteColumns {
        public static final String TABLE_NAME = "route";
        public static final String URI_PATH = "route";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + URI_PATH);
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + URI_PATH;
        public static final String CONTENT_TRACK_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + URI_PATH;

        private RouteTable() {}
    }

    public interface LocationColumns {
        String ID = BaseColumns._ID;
        String TRACK_ID = "track_id";
        String NAME = "name";
        String LAT = "lat";
        String LON = "lon";
        String CREATED_ON = "created_on";
    }

    public static final class LocationTable implements LocationColumns {
        public static final String TABLE_NAME = "location";
        public static final String URI_PATH = "location";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + URI_PATH);
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + URI_PATH;
        public static final String CONTENT_LOCATION_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + URI_PATH;

        private LocationTable() {}
    }
}
