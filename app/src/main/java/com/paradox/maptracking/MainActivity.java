package com.paradox.maptracking;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.paradox.maptracking.data.DatabaseContract;

public class MainActivity extends AppCompatActivity {

    private ListView lvRoute;
    private FloatingActionButton fabNewRoute;

    private Intent mIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSupportActionBar((Toolbar) findViewById(R.id.tToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("RUTA");

        lvRoute = (ListView) findViewById(R.id.lvTrack);
        lvRoute.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor c = (Cursor) lvRoute.getItemAtPosition(position);
                int track_id = c.getInt(c.getColumnIndex(DatabaseContract.RouteTable.ID));
                mIntent = new Intent(MainActivity.this, GoogleMapActivity.class);
                mIntent.putExtra("track_id", String.valueOf(track_id));
                startActivity(mIntent);
            }
        });

        setList();

        fabNewRoute = (FloatingActionButton) findViewById(R.id.fabNewTrack);
        fabNewRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIntent = new Intent(MainActivity.this, RouteActivity.class);
                startActivity(mIntent);
            }
        });
    }

    private void setList() {
        CursorAdapter adapter = new CursorAdapter(this, getContentResolver().query(DatabaseContract.RouteTable.CONTENT_URI, null, null, null, null)) {
            @Override
            public View newView(Context context, Cursor cursor, ViewGroup parent) {
                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.listview_item_main, parent, false);
                return view;
            }

            @Override
            public void bindView(View view, Context context, Cursor cursor) {
                TextView tvRouteCount = (TextView) view.findViewById(R.id.tvMainNumber);
                TextView tvName = (TextView) view.findViewById(R.id.tvMainName);

                int number = cursor.getInt(cursor.getColumnIndex(DatabaseContract.RouteTable.COUNT));
                tvRouteCount.setText(String.valueOf(number) + " PUNTOS");
                tvName.setText(cursor.getString(cursor.getColumnIndex(DatabaseContract.RouteTable.NAME)));
            }
        };

        lvRoute.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
